package edu.oop.java.intro;

import edu.oop.java.intro.geometry.Circle;
import edu.oop.java.intro.geometry.Color;
import edu.oop.java.intro.geometry.Point;
import edu.oop.java.intro.geometry.Shape;
import edu.oop.java.intro.geometry.Triangle;

import java.util.ArrayList;
import java.util.List;

public class Intro {

    public static void main(String[] args) {
        final Point p = new Point(1, 2);

        final Circle c = new Circle(Color.GREEN, new Point(3, 4), 5);
        final Triangle t = new Triangle(Color.RED, new Point(0, 0), new Point(5, 0), new Point(0, 5));

        final List<Shape> shapes = new ArrayList<>();
        shapes.add(c);
        shapes.add(t);

        for (Shape shape : shapes) {
            shape.move(p);
        }

        /* Expected console output is
            Called from circle to move to Point{x=1.0, y=2.0}
            Called from triangle to move to Point{x=1.0, y=2.0}
         */
    }
}
