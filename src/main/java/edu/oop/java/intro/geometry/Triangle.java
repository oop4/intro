package edu.oop.java.intro.geometry;

public class Triangle extends Shape {

    private Point a;
    private Point b;
    private Point c;

    public Triangle(Color color, Point a, Point b, Point c) {
        super(color);

        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public void move(Point newPosition) {
        // TODO: implement movement
        System.out.println("Called from triangle to move to " + newPosition);
    }
}
