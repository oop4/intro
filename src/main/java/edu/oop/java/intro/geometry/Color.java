package edu.oop.java.intro.geometry;

public enum Color {

    RED,
    GREEN,
    BLUE,
    BLACK
}
