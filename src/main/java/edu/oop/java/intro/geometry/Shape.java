package edu.oop.java.intro.geometry;

public abstract class Shape {

    protected Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void move(Point newPosition);
}
