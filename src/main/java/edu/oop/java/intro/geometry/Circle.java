package edu.oop.java.intro.geometry;

public class Circle extends Shape {

    private Point center;
    private double radius;

    public Circle(Color color, Point center, double radius) {
        super(color);

        this.center = center;
        this.radius = radius;
    }

    public void move(Point newPosition) {
        // TODO: implement movement
        System.out.println("Called from circle to move to " + newPosition);
    }
}
